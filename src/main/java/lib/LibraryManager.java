package lib;

import lib.enums.BookCategory;
import lib.enums.VinylCategory;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LibraryManager {
    private List<Item> stuff;

    public LibraryManager() {
        stuff = createList();
    }

    public List<Item> createList() {

        List<Item> stuff = new ArrayList<Item>();

        stuff.add(new Book("Tomaszkowo", "Zdzichu", "Co to tam teraz", 156, BookCategory.Comedy));
        stuff.add(new Vinyl("34 Ciastek", "MC Hammer", "Oppa, oppa", VinylCategory.Blues));
        stuff.add(new Book("Henryk Sienkiewicz", "Ogniem i mieczem", "Super story", 123, BookCategory.Drama));
        stuff.add(new Book("Balladyna", "Juliusz Słowacki", "Ultra sad poetry", 12, BookCategory.Drama));
        stuff.add(new Book("Morderstwo w Orient Expressie", "Agatha Cristie", "Detective stuff", 34, BookCategory.Adventure));
        stuff.add(new Vinyl("Elektryczne kwiaty", "Modern music", "Drum and baass and more", VinylCategory.Soul));
        stuff.add(new Vinyl("Czlowiek z liściem", "Kuba Sienkiewicz", "Polska muzyka rozrywkowa", VinylCategory.Blues));
        stuff.add(new Book("Smierć na Nilu", "Agatha Cristie", "Agatha Christie stuff", 34, BookCategory.Horror));
        stuff.add(new Book("Oda do marnosci", "Juliusz Konieczko", "Classic", 37, BookCategory.Poem));
        stuff.add(new Book("Quo vadis", "Henryk Sienkiewicz", "Obligatory", 234, BookCategory.Adventure));
        stuff.add(new Book("Historia Polski", "Roman Brona", "I dont really know", 65, BookCategory.Adventure));

        return stuff;
    }

    public void showAll() {

        for (int i = 0; i < stuff.size(); i++) {
            int x = i + 1;
            Item item = stuff.get(i);
            if (item instanceof Vinyl) {
                Vinyl vinyl = (Vinyl) item;
                System.out.println(x + " " + stuff.get(i).getTille() + " " + stuff.get(i).getAuthor() + " " + stuff.get(i).getDescription() + " " + stuff.get(i).getCategory());
            }
            if (item instanceof Book) {
                Book book = (Book) item;
                System.out.println(x + " " + stuff.get(i).getTille() + " " + stuff.get(i).getAuthor() + " " +
                        stuff.get(i).getDescription() + " " + stuff.get(i).getNumberOfPages() + " " + stuff.get(i).getCategory());
            }
        }
    }

    public void sortAllByTitle() {
        List<Item> sortex = new ArrayList<>(stuff);
        sortex.sort(Comparator.comparing(Item::getTille));
        for (int i = 0; i < sortex.size(); i++) {
            Item item = sortex.get(i);
            if (item instanceof Vinyl) {
                Vinyl vinyl = (Vinyl) item;
                System.out.println(sortex.get(i).getTille() + " " + sortex.get(i).getAuthor() + " " +
                        sortex.get(i).getDescription() + " " + sortex.get(i).getCategory());
            }
            if (item instanceof Book) {
                Book book = (Book) item;
                System.out.println(sortex.get(i).getTille() + " " + sortex.get(i).getAuthor() + " " +
                        sortex.get(i).getDescription() + " " + sortex.get(i).getNumberOfPages() + " " +
                        sortex.get(i).getCategory());
            }
        }
    }

    public void showVinyls() {
        for (int i = 0; i < stuff.size(); i++) {
            if (stuff.get(i).getCategory() instanceof VinylCategory) {
                System.out.println(stuff.get(i).getTille() + " " + stuff.get(i).getAuthor() + " " +
                        stuff.get(i).getDescription() + " " + stuff.get(i).getCategory());
            }
        }
    }

    public void sortVinylByTitle() {
        List<Item> sortex = new ArrayList<>(stuff);
        sortex.sort(Comparator.comparing(Item::getTille));
        for (int i = 0; i < sortex.size(); i++) {
            Item item = sortex.get(i);
            if (item instanceof Vinyl) {
                Vinyl vinyl = (Vinyl) item;
                System.out.println(sortex.get(i).getTille() + " " + sortex.get(i).getAuthor() + " " +
                        sortex.get(i).getDescription() + " " + sortex.get(i).getCategory());
            }
        }
    }

    public void showBooks() {
        for (int i = 0; i < stuff.size(); i++) {
            if (stuff.get(i).getCategory() instanceof BookCategory) {
                System.out.println(stuff.get(i).getTille() + " " + stuff.get(i).getAuthor() + " " +
                        stuff.get(i).getDescription() + " " + stuff.get(i).getNumberOfPages() + " " +
                        stuff.get(i).getCategory());
            }
        }
    }

    public void addBook() {

        Scanner scaner = new Scanner(System.in);

        System.out.println("Title :");
        String title = scaner.next();

        System.out.println("Author :");
        String author = scaner.next();

        System.out.println("Description :");
        String description = scaner.next();

        System.out.println("Number of pages :");
        String nop = scaner.next();

        System.out.println("Category :");
        System.out.println("1 - Comedy");
        System.out.println("2 - Drama");
        System.out.println("3 - Horror");
        System.out.println("4 - Adventure");

        int reader = scaner.nextInt();

        switch (reader) {
            case 1:
                stuff.add(new Book(title, author, description, Integer.valueOf(nop), BookCategory.Comedy));
                break;
            case 2:
                stuff.add(new Book(title, author, description, Integer.valueOf(nop), BookCategory.Drama));
                break;
            case 3:
                stuff.add(new Book(title, author, description, Integer.valueOf(nop), BookCategory.Horror));
                break;
            case 4:
                stuff.add(new Book(title, author, description, Integer.valueOf(nop), BookCategory.Adventure));
                break;
        }
    }

    public void addBookFromFile() {
        File excelFile = new File("kontakty.xlsx");
        try {
            List<String> listka = new ArrayList<>();
            FileInputStream fis = new FileInputStream(excelFile);
            XSSFWorkbook workbook = new XSSFWorkbook(fis);

            // we get first sheet
            XSSFSheet sheeet = workbook.getSheetAt(0);

            // we iterate on rows (wiersze)
            Iterator<Row> rowIt = sheeet.iterator();

            while (rowIt.hasNext()) {
                Row row = rowIt.next();
                // iterate on cells for the current row
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    listka.add(String.valueOf(cell));
                    if (listka.size() == 5) {
                        stuff.add(new Book(listka.get(0), listka.get(1), listka.get(2), Integer.valueOf((listka.get(3))), BookCategory.valueOf((listka.get(4)))));
                        listka.removeAll(listka);
                    }
                }
            }
            workbook.close();
            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku");
        } catch (IOException i) {
            System.out.println("Nie znaleziono pliku");
        }
    }

    public boolean deleteItem(int i) {
        System.out.println("Skasowales pozycje : " + stuff.get(i - 1).getAuthor() + " " + stuff.get(i - 1).getTille());
        stuff.remove(i - 1);
        return true;
    }
    public void sortBooksByTitle() {
        List<Item> sortex = new ArrayList<>(stuff);
        sortex.sort(Comparator.comparing(Item::getTille));
        for (int i = 0; i < sortex.size(); i++) {
            Item item = sortex.get(i);
            if (item instanceof Book) {
                //Book book = (Book) item;
                System.out.println(sortex.get(i).getTille() + " " + sortex.get(i).getAuthor() + " " +
                        sortex.get(i).getDescription() + " " + sortex.get(i).getNumberOfPages() + " " +
                        sortex.get(i).getCategory());
            }
        }
    }

    public void showMenu() {
        System.out.println("1. Pokaż wszystko");
        System.out.println("2. Pokaż książki");
        System.out.println("3. Pokaż płyty");
        System.out.println("4. Sortuj po tytule");
        System.out.println("5. Dodaj książkę/płytę");
        System.out.println("6. Usuń książkę/płytę");
        System.out.println("7. Załaduj bibliotekę zewnetrzną");
        System.out.println("0. Zakończ");
    }
}