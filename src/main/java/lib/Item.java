package lib;

public class Item {

    private String tille;
    private String author;
    private String description;
    private Areable category;


    public Item(String tille, String author, String description) {
        this.tille = tille;
        this.author = author;
        this.description = description;
    }

    public Item() {
    }

    public String getTille() {
        return tille;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public int getNumberOfPages() {
        return getNumberOfPages();
    }

    public Areable getCategory()
    {
        return category;
    }
}
