package lib;

import java.util.Scanner;

public class GuiManager {
    public void menuStart() {
        LibraryManager test = new LibraryManager();
        test.showMenu();
        Scanner scanner = new Scanner(System.in);
        int pointer = 1;
        int del;
        while (pointer > 0) {
            pointer = scanner.nextInt();
            switch (pointer) {
                case 1:
                    test.showAll();
                    System.out.println("Press '8' to main menu");
                    break;
                case 2:
                    test.showBooks();
                    System.out.println("Press '8' to main menu");
                    break;
                case 3:
                    test.showVinyls();
                    System.out.println("Press '8' to main menu");
                    break;
                case 4:
                    test.sortBooksByTitle();
                    System.out.println("Press '8' to main menu");
                    break;
                case 5:
                    test.addBook();
                    System.out.println("Press '8' to main menu");
                    break;
                case 6:
                    System.out.println("Podaj numer do skasowania");
                    del = scanner.nextInt();
                    test.deleteItem(del);
                    System.out.println("Press '8' to main menu");
                    break;
                case 7:
                    test.addBookFromFile();
                    System.out.println("Loaded");
                    System.out.println("Press '8' to main menu");
                    break;
                case 8:
                    test.showMenu();
                    break;

            }
        }
    }
}
