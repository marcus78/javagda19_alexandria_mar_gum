package lib;

import lib.enums.VinylCategory;

public class Vinyl extends Item{

    private VinylCategory category;

    public Vinyl(String tille, String author, String description, VinylCategory category) {
        super(tille, author, description);
        this.category = category;
    }

    public VinylCategory getCategory()
    {
        return category;
    }
}
