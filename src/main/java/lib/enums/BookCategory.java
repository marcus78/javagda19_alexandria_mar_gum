package lib.enums;

import lib.Areable;

public enum BookCategory implements Areable {
    Adventure, Comedy, Drama, Horror, Poem, Romantic, SciFi, Ciminal, Fantasy;
    public BookCategory category;

    public BookCategory getCategory() {
        return category;
    }
}
