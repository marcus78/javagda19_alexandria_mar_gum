package lib.enums;

import lib.Areable;

public enum VinylCategory implements Areable {
    Soul, Techno, Punk, Metel, Folk, Blues, Classic;
    public VinylCategory category;

    public VinylCategory getCategory()
    {
        return category;
    }

}
