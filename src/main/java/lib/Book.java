package lib;

import lib.enums.BookCategory;

public class Book extends Item {
    private int numberOfPages;
    private BookCategory category;

    public Book(String tille, String author, String description, int numberOfPages, BookCategory category) {
        super(tille, author, description);
        this.numberOfPages = numberOfPages;
        this.category = category;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public BookCategory getCategory() {
        return category;
    }
}
